SELECT yt_id, counter FROM v2t WHERE tag_id=(SELECT id FROM tags WHERE name='frustrated')
ORDER BY counter DESC LIMIT 100

-- je�li jest takie video i taki tag w bazie:
INSERT INTO v2t(yt_id, tag_id, counter)  VALUES ('QTA4FHafR_o', (SELECT id FROM tags WHERE name = 'angry'), 1)
  ON DUPLICATE KEY UPDATE counter=counter+1;
 
-- magic query (music by tags)
 
 SELECT yt_id, counter FROM v2t WHERE tag_id IN (SELECT id FROM tags WHERE name IN ('bad', 'sad', 'angry', 'bitter', 'losing')); 
 
 SELECT yt_id, counter FROM v2t WHERE tag_id IN (SELECT id FROM tags WHERE name IN ('bad', 'sad', 'angry', 'bitter', 'losing')) GROUP BY yt_id;
 
 SELECT yt_id, SUM(counter) FROM v2t WHERE tag_id IN (SELECT id FROM tags
WHERE name IN ('bad', 'sad', 'angry', 'bitter', 'losing')) GROUP BY yt_id;

SELECT yt_id, SUM(counter) as counters FROM v2t WHERE tag_id IN (SELECT i
d FROM tags WHERE name IN ('bad', 'sad', 'angry', 'bitter', 'losing')) GROUP BY
yt_id ORDER BY counters DESC;

SELECT yt_id, SUM(counter) as counters FROM v2t WHERE tag_id IN (SELECT id FROM tags WHERE name IN ('bad', 'sad', 'angry', 'bitter', 'losing') GROUP BY yt_id ORDER BY counters DESC;

-- untagged videos
SELECT yt_id FROM videos WHERE yt_id NOT IN (SELECT yt_id FROM v2t GROUP BY yt_id);