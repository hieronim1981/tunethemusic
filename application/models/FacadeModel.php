<?php

class FacadeModel
{
    protected static $_instance = null;    
    
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    protected static function getPDO()
    {        
        //return new PDO(
    }
    
    public static function getAllTags()
    {
        $allTags = array();
        
        try
        {
              $pdo = FacadeModel::getInstance()->getPDO();
              $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

              $stmt = $pdo -> query('
                    SELECT tags.name FROM tags;
                    ');

              while($row = $stmt -> fetch())
              {
                  array_push($allTags, $row['name']);
              }

              $stmt -> closeCursor();
              
              return $allTags;
        }
        catch(PDOException $e)
        {
        }
    }
    
    public static function getTagCountersByVideoId($id)
    {
        $tags = array();
     
        if (strlen($id)==11)
        {
           try
           {
                  $pdo = FacadeModel::getInstance()->getPDO();
                  $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                  $stmt = $pdo -> query('
                        SELECT tags.name, v2t.counter FROM v2t JOIN tags ON v2t.yt_id = ' . $pdo->quote($id) . ' AND v2t.tag_id = tags.id
                        ');

                  while($row = $stmt -> fetch())
                  {
                      array_push($tags,
                              array('name' => $row['name'], 'counter' => $row['counter']));
                  }

                  $stmt -> closeCursor();

                  return $tags;
           }
           catch(PDOException $e)
           {                
           }
        }
    }
    
    public static function incrementTag($tagName, $videoId)
    {        
        try
        {
              $pdo = FacadeModel::getInstance()->getPDO();
              $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              
              // jeśli nie ma video albo tagu w bazie = SQLERROR (łapany)
              // poprawić!
              $queryStr = 'INSERT INTO v2t(yt_id, tag_id, counter) VALUES ((SELECT yt_id FROM videos WHERE yt_id = '
                .$pdo->quote($videoId).'), (SELECT id FROM tags WHERE name = '
                .$pdo->quote($tagName).'), 1) ON DUPLICATE KEY UPDATE counter=counter+1;';
              
              $stmt = $pdo -> exec($queryStr);
        }
        catch(PDOException $e)
        {
        }
    }
    
    public static function musicByTags($userTags, $feedSize=null, $feeded=null)
    {
        $feed = array();
        if(!$feedSize or $feedSize<=0)
            $feedSize=20;
        if(!$feeded)
            $feeded=0;
        
        try
        {
              $pdo = FacadeModel::getInstance()->getPDO();
              $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              
              $arrayTags = explode(",", $userTags);
              $params = "";
              $i=0;
              for(; $i<sizeof($arrayTags)-1; ++$i)
                $params .= $pdo->quote($arrayTags[$i]).", ";
              $params .= $pdo->quote($arrayTags[$i]);              
              
              $queryStr = "SELECT yt_id, SUM(counter) as counters FROM v2t WHERE tag_id IN (SELECT id FROM tags WHERE name IN (";
              $queryStr .= $params;    
              $queryStr .= ")) GROUP BY yt_id ORDER BY counters DESC "
              . "LIMIT ".strval(20*intval($feeded)).", ".strval($feedSize).";";

              $stmt = $pdo -> query($queryStr);
              
              while($row = $stmt -> fetch())
              {
                    array_push($feed, array("id" => $row['yt_id'],
                        "counters" => $row['counters']));
              }
              
              $stmt -> closeCursor();
              
              return $feed;
        }
        catch(PDOException $e)
        {            
        }
    }
    
    public static function selectUntaggedVideos($feedSize=null, $feeded=null)
    {
        $feed = array();
        if(!$feedSize or $feedSize<=0)
            $feedSize=20;
        if(!$feeded)
            $feeded=0;
        
        try
        {
              $pdo = FacadeModel::getInstance()->getPDO();
              $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              
              $queryStr = "SELECT yt_id FROM videos WHERE yt_id NOT IN (SELECT yt_id FROM v2t GROUP BY yt_id)"
              . "LIMIT ".strval(20*intval($feeded)).", ".strval($feedSize).";";
              
              $stmt = $pdo -> query($queryStr);
              
              while($row = $stmt -> fetch())
              {
                  array_push($feed, array("id" => $row['yt_id']));
              }
              
              $stmt -> closeCursor();
              
              return $feed;
        }
        catch(PDOException $e)
        {
        }
    }
    
    public static function topByTag($tagName)
    {
        $feed = array();
        
        try
        {
              $pdo = FacadeModel::getInstance()->getPDO();
              $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

              $stmt = $pdo -> query('
                    SELECT yt_id, counter FROM v2t WHERE tag_id=(SELECT id FROM tags WHERE name='
                      . $pdo->quote($tagName) .
                      ') ORDER BY counter DESC LIMIT 100;
                    ');

              while($row = $stmt -> fetch())
              {
                  array_push($feed, array("id" => $row['yt_id']));
                  array_push($feed, array("counter" => $row['counter']));
              }

              $stmt -> closeCursor();
         
              return $feed;
        }
        catch(PDOException $e)
        {
        }
    }
    
    public static function addVideo($videoId, $title = null)
    {   
        try
        {
              if($title == null)
                $title = FacadeModel::getInstance()->getYTTitle($videoId);
              if($title)
              {
                $pdo = FacadeModel::getInstance()->getPDO();
                $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $queryStr =
                      'INSERT INTO videos(yt_id, title)  VALUES ('                       
                        . $pdo->quote($videoId) . ', '
                        . $pdo->quote($title) . ')
                      ON DUPLICATE KEY UPDATE title=' . $pdo->quote($title);

                $pdo -> exec($queryStr);
              }
        }
        catch(PDOException $e)
        {
        }
    }
    
    public static function getYTTitle($videoId)
    {
        try {
            $client = new Zend_Http_Client();
            $client->setUri('http://gdata.youtube.com/feeds/api/videos/' . $videoId);
            $client->setParameterGet(array(
                'v' => '2',
                'alt'  => 'json',
                'fields' => 'title'
            ));

            $response = $client->request('GET');
            if($response->getStatus()!=200)
                return null;

            $json_object = json_decode($response->getBody(), true);
            return $json_object['entry']['title']['$t'];
        }
        catch (Exception $e)
        {            
        }
    }
    
    protected static function addVideos($videosArray)
    {
        // one query for imports
        try
        {
            $pdo = FacadeModel::getInstance()->getPDO();
            $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $queryStr =
              'INSERT INTO videos(yt_id, title) VALUES ';

                $i=0;
                for(; $i<sizeof($videosArray)-1; ++$i)
                    $queryStr = $queryStr .
                '(' . $pdo->quote($videosArray[$i]['id']) . ', ' . $pdo->quote($videosArray[$i]['title']) . '), ';

                $queryStr = $queryStr .
                '(' . $pdo->quote($videosArray[$i]['id']) . ', ' . $pdo->quote($videosArray[$i]['title']) .
                ') ON DUPLICATE KEY UPDATE title=VALUES(title);';

            $pdo->exec($queryStr);
        }
        catch(PDOException $e)
        {
        }
    }
    
    public static function importFavourites($userName)
    {
        try {
            $client = new Zend_Http_Client();
            $client->setUri("http://gdata.youtube.com/feeds/api/users/".$userName."/favorites");

            $toFetch = 50; // 50 is the max value of YouTube api
            $offset = -49; // one base index..        

            $resultSet = array();
            $q = 1;        
            while(true) {
                $offset += $toFetch;
                $client->setParameterGet(array(
                    'v' => '2',
                    'alt'  => 'json',
                    'fields' => 'entry(title,media:group(yt:videoid))',
                    'start-index' => $offset,
                    'max-results' => $toFetch
                ));

                $response = $client->request('GET');
                if($response->getStatus()==200
                        and ($json_object = json_decode($response->getBody(), true))
                        and array_key_exists('feed', $json_object)
                        and array_key_exists('entry', $json_object['feed']) )
                {
                    foreach($json_object['feed']['entry'] as $vid)
                    {
                        $title = $vid['title']['$t'];
                        $id = $vid['media$group']['yt$videoid']['$t'];                        
                        array_push($resultSet, array('title' => $title, 'id' => $id));                        
                        $q= 1+$q;
                    }
                }
                else
                    break;
            }

            FacadeModel::getInstance()->addVideos($resultSet);
        }
        catch (Exception $e)
        {            
        }
    }
    
    public static function importUploads($userName)
    {
        try {
            $client = new Zend_Http_Client();
            $client->setUri("http://gdata.youtube.com/feeds/api/users/".$userName."/uploads");

            $toFetch = 50; // 50 is the max value of YouTube api
            $offset = -49; // one base index..        

            $resultSet = array();
            $q = 1;        
            while(true) {
                $offset += $toFetch;
                $client->setParameterGet(array(
                    'v' => '2',
                    'alt'  => 'json',
                    'fields' => 'entry(title,media:group(yt:videoid))',
                    'start-index' => $offset,
                    'max-results' => $toFetch
                ));

                $response = $client->request('GET');
                if($response->getStatus()==200
                        and ($json_object = json_decode($response->getBody(), true))
                        and array_key_exists('feed', $json_object)
                        and array_key_exists('entry', $json_object['feed']) )
                {
                    foreach($json_object['feed']['entry'] as $vid)
                    {
                        $title = $vid['title']['$t'];
                        $id = $vid['media$group']['yt$videoid']['$t'];                        
                        array_push($resultSet, array('title' => $title, 'id' => $id));                        
                        $q= 1+$q;
                    }
                }
                else
                    break;
            }

            FacadeModel::getInstance()->addVideos($resultSet);
        }
        catch (Exception $e)
        {            
        }
        
//        echo 'Favourites ' . $userName;
//        require_once 'Zend/Loader.php';
//        Zend_Loader::loadClass('Zend_Gdata_AuthSub');
//        Zend_Loader::loadClass('Zend_Gdata_App_Exception');
//        Zend_Loader::loadClass('Zend_Gdata_YouTube');
//        
//        
////        $adapter = new Zend_Http_Client_Adapter_Curl();
////        $adapter->setCurlOption(CURLOPT_SSL_VERIFYPEER, false);
////        $httpClient = new Zend_Gdata_HttpClient();
////        $httpClient->setAdapter($adapter);
////
////        $client = Zend_Gdata_ClientLogin::getHttpClient(null, null, null, $httpClient);
////        
////        $youTubeService = new Zend_Gdata_YouTube($client);
//        $youTubeService = new Zend_Gdata_YouTube();
//        $q = 1;        
//        $feed = $youTubeService->getUserFavorites('rov3rPL');
////        do {
////            foreach ($feed as $entry) {
////                $videoId = $entry->getVideoId();
////                echo $q . '    ' . $videoId . '<hr />';
////                $q= 1+$q;
////            }
////        } while($feed=$feed->getNextLink()->getNextFeed());
//                
//        echo $feed->getNextLink();
    }
}

?>
