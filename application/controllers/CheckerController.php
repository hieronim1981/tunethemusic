<?php

class CheckerController extends Zend_Controller_Action
{
    public function init()
    {
        // Get the context switcher helper
        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        // We want to have a json and an xml context available for action
        $contextSwitch->addActionContext('get-tag-counters-by-video-id', array ('xml', 'json'))
                ->setAutoJsonSerialization(true)
                ->initContext();
    }
    
    public function indexAction()
    {
        
    }
        
    public function getTagCountersByVideoIdAction()
    {             
        $id = $this->getParam('id');
        
        require_once 'FacadeModel.php';
        $tags = FacadeModel::getInstance()->getTagCountersByVideoId($id);
        
        $this->view->assign('id', $id);
        $this->view->assign('tags', $tags);
    }
    
}

