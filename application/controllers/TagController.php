<?php

class TagController extends Zend_Controller_Action
{
    public function init()
    {
        // Get the context switcher helper
        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        // We want to have a json and an xml context available for action
        $contextSwitch->addActionContext('top-by-tag', array ('xml', 'json'))
                ->setAutoJsonSerialization(true)
                ->initContext();
    }
    
    public function indexAction()
    {
        
    }
    
    public function topByTagAction()
    {
        $tagName = $this->getParam('tag');
                
        require_once 'FacadeModel.php';
        $feed = FacadeModel::getInstance()->topByTag($tagName);
    
        $this->view->assign('tag', $tagName);
        $this->view->assign('feed', $feed);
    }
}

