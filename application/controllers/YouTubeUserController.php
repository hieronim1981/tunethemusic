<?php

class YouTubeUserController extends Zend_Controller_Action
{

    public function indexAction()
    {   
    }
    
    public function importFavouritesAction()
    {
        $userName = $this->getParam('user');
                
        require_once 'FacadeModel.php';
        FacadeModel::getInstance()->importFavourites($userName);
    }
    
    public function importUploadsAction()
    {
        $userName = $this->getParam('user');
                
        require_once 'FacadeModel.php';
        FacadeModel::getInstance()->importUploads($userName);
    }

}

