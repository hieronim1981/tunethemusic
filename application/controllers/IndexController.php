<?php

class IndexController extends Zend_Controller_Action
{
    public function init()
    {        
        // Get the context switcher helper
        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        // We want to have a json and an xml context available for action
        $contextSwitch->addActionContext('get-all-tags', array ('xml', 'json'))
                ->setAutoJsonSerialization(true)
                ->initContext();
        
        $contextSwitch->addActionContext('music-by-tags', array ('xml', 'json'))
                ->setAutoJsonSerialization(true)
                ->initContext();
        
        $contextSwitch->addActionContext('increment-tag', array ('xml', 'json'))
                ->setAutoJsonSerialization(true)
                ->initContext();
    }
    
    public function indexAction()
    {
        
    }
    
    public function getAllTagsAction()
    {
        require_once 'FacadeModel.php';
        $allTags = FacadeModel::getInstance()->getAllTags();
                
        $this->view->assign('tags', $allTags);
    }
    
    public function musicByTagsAction()
    {
        $userTags = $this->getParam('tags');
        $feeded = $this->getParam('feeded');
                        
        require_once 'FacadeModel.php';
        $feed = FacadeModel::getInstance()->musicByTags($userTags, $feeded);
        
        if(sizeof($feed)!=20) {
            $feed2 = FacadeModel::getInstance()->selectUntaggedVideos(20-sizeof($feed), $feeded);
            $feed = array_merge($feed, $feed2);
        }
        $this->view->assign('tags', $userTags);
        $this->view->assign('feed', $feed);
    }
    
    public function incrementTagAction()
    {
        $videoId = $this->getParam('videoId');
        $tagName = $this->getParam('tagName');
                
        require_once 'FacadeModel.php';
        FacadeModel::getInstance()->incrementTag($tagName, $videoId);
    }   
    
    public function addVideoAction()
    {
        $videoId = $this->getParam('videoId');        
        if($videoId)
        {
            require_once 'FacadeModel.php';
            FacadeModel::getInstance()->addVideo($videoId);
        }
    }
    
}
