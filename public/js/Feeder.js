function Feeder(queryObject, onFeedComplete)
{
    // query["type"]
    // query["feedSize"]
    // query["params"][...]
    //
    this.query = queryObject;
    this.onFeedComplete = onFeedComplete;
    
    this.getQueryString = function() {
        return "http://localhost/tuneTheMusic/public/tag/top-by-tag?format=json&tag=angry";
    }
    
    this.advanceQueryParams = function() {
        
    }
    
    this.createRequestObject = function() {        
        if (window.XMLHttpRequest)
          {	// code for IE7+, Firefox, Chrome, Opera, Safari
                return new XMLHttpRequest();
          }
        else
          {	// code for IE6, IE5
                return new ActiveXObject("Microsoft.XMLHTTP");
          }
    }
}

Feeder.prototype.GetQueryObject = function()
{
    return query;
}

Feeder.prototype.Feed = function(newQuery)
{    
    if(newQuery)
        this.query=newQuery;
 
    var xmlhttp = this.createRequestObject();
    xmlhttp.onreadystatechange=function(caller)
    {
        return function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                var JSON_object = {};
                JSON_object = JSON.parse(xmlhttp.responseText);
                
                caller.onFeedComplete(JSON_object.feed);                
            }
        }
    }(this);
    
    url = this.getQueryString();
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    
    this.advanceQueryParams();
}

