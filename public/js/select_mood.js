// copying values cloud to select buttons
$("#videoValuesCloud").ready(
    function(){
        $("#selectMoodButtons").html($("#videoValuesCloud>").clone());        
        $("#selectMoodButtons>ul>li>button").each(
            function(i,o){
                $(o).attr("class", "tagbutton");
            }
        );
        
        $("#selectMoodButtons>ul>li>button").click(
            function(ev){
                var $el=$(ev.target);
                $el.toggleClass("pressed");
                addOrDeleteMood($el.parent().attr('title'));
            }
        );
    }
);       

var mood_container = {};
var moodChanged;
function addOrDeleteMood(name)
{
    if(mood_container[name])
        mood_container[name]=0;
    else
        mood_container[name]=1;
    moodChanged = true;
}

function moodsToString()
{
    var s = "";
    for(var i in mood_container)
        if(mood_container[i]==1)
            s += i + ",";
    return s.substring(0, s.length-1);
}

function go()
{
    $("#selectMoodButtons").slideToggle();
    
    if ($("#goButton").text() == "Change mood") {
       $("#goButton").text("Start listening");
       $("#goButton").css("margin-top", "0px");
       return;
    }       
    
    $("#goButton").text("Change mood");    
    $("#goButton").css("margin-top", "-"+$("#goButton").outerHeight(true)+"px");
    
    if(moodChanged) {        
        query(moodsToString(), fillVideoBar);
        moodChanged = false;
        feeded = 0;
    }
}

