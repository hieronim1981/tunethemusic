function checkVideo() {
    var videoId = youtube_parser(document.getElementById('videoIdInput').value);
    if(videoId)
        valuesCloudObject.GetTagCountersByVideoId(videoId);
}

function youtube_parser(url){
    if(url.length==11) return url;
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match&&match[2].length==11){
        return match[2];
    }else{
        alert('Not YouTube video link');        
    }
}

//function unpresentVideo(videoId)
//{
//    alert('Video nie ma w bazie, dodać?');
//}

function addVideo(videoId)
{
    var xmlhttp = createRequestObject();
    
    var url = document.domain;
    if (url.match(/^(.*)(localhost)(.*)/))
    {        
        url = "http://localhost/tuneTheMusic/public/index/add-video?videoId="+videoId;
    }
    else
        url = "index/add-video?videoId="+videoId;
    
    xmlhttp.open("POST", url, true);
    xmlhttp.send();
}
