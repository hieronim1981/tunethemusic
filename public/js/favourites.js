var userName = ''; // wprowadzona nazwa użytkownika YT
var offset = 1; // start-index is one based!
var fetchedVideos = 0;
var feedSize = 10; // max. 50 (YT restriction)

var url = ''; // TESTS


function getUserFavourites()
{
    var xmlhttp = createRequestObject();
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {   
            var JSON_object = {};
            JSON_object = JSON.parse(xmlhttp.responseText);
            fetchedVideos = JSON_object.feed.entry.length;

            simpleConsumeFeed(JSON_object.feed.entry);
        }
    }
    //&fields=entry(media:group)
    //&fields=entry(title,link)
    //&fields=entry(title,media:group(yt:videoid))
    url = "http://gdata.youtube.com/feeds/api/users/"+userName+"/favorites?alt=json&fields=entry(title,media:group(yt:videoid))&start-index="+offset+"&max-results="+feedSize+"&v=2";
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

var fyd = new Array();
var ind = -1;
function simpleConsumeFeed(feed)
{
    fyd.push.apply(fyd, feed);
    
    tablica = document.getElementById('tablica');
    tablica.value='';
    for(var i=0; i<fetchedVideos; ++i)
        tablica.value += feed[i].title.$t + "\n";

    tablica.value += url;
}

function prevEntry()
{
    if(ind>0)
    {
        --ind;
        entryChange(fyd[ind].media$group.yt$videoid.$t, fyd[ind].title.$t);
    }
}

function entryChange(videoId, videoTitle)
{
    $("#titleText").text(videoTitle);
        $("#thumb").attr('src', 
        "http://img.youtube.com/vi/"+videoId+"/mqdefault.jpg");
  
    valuesCloudObject.GetTagCountersByVideoId(videoId);
}

function nextEntry()
{
    if(ind<fyd.length-1)
    {
        ++ind;
        entryChange(fyd[ind].media$group.yt$videoid.$t, fyd[ind].title.$t);
        
        if(ind==(fyd.length-1))
            nextFeed();
    }
}

function nextFeed()
{
    if(userName && fetchedVideos == feedSize) { offset+=fetchedVideos; getUserFavourites(); }
}

function prevFeed()
{
    if(userName && offset>1) { offset-=feedSize; getUserFavourites(); }
}