var valuesCloudObject = new ValuesCloud("videoValuesCloud");

function ValuesCloud(cloudId) {
    this.cloudId = cloudId;
    this.videoId = "";
    
    $.tagcloud.defaults.type = "sphere";
    $.tagcloud.defaults.power = 0.3;
    $.tagcloud.defaults.sizemin = 10;
    $.tagcloud.defaults.sizemax = 24;
    $.tagcloud.defaults.colormin = "FFFFFF";
    $.tagcloud.defaults.colormax = "FFFFFF";
    
    $("#"+this.cloudId+">ul").ready(
        function() {
            $("#"+cloudId+">ul").click(
                function(ev){
                    var $el=$(ev.target);
                    if ( $el.is('button') ){
                        $el.parent().val($el.parent().attr('value')+1);
                        $el.html($el.parent().attr('title')+'('+$el.parent().attr('value')+')');
                
                        //$("#"+cloudId+">ul>li").tsort({attr:"value"});
                        $("#"+cloudId+">ul").tagcloud();
                        
                        valuesCloudObject.HitTag($el.parent().attr('title'));
                        return false;
                    }
                 }
             );
        }
    );    
}

ValuesCloud.prototype.GetTagCountersByVideoId = function(videoId) {
    this.videoId = videoId;
    var xmlhttp = createRequestObject();
    xmlhttp.onreadystatechange = function(caller) {
        return function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                var JSON_object = {};
                JSON_object = JSON.parse(xmlhttp.responseText);
                if(videoId==JSON_object.id)
                { 
                    if(JSON_object.tags.length)
                        caller.PutValuesIntoCloud(JSON_object.tags);
                    else
                        caller.ResetValuesInCloud();
                    
                    // gdy jest odwrotnie chmura się "wykrzacza" :/
                    $("#"+caller.cloudId).slideDown();
                    $("#"+caller.cloudId+">ul>li").tsort({order:'desc',attr:"value"});
                    $("#"+caller.cloudId+">ul").tagcloud();
                }
            }
        }
    }(this);
    
    var url = document.domain;
    if (url.match(/^(.*)(localhost)(.*)/))
    {
        url = "http://localhost/tuneTheMusic/public/checker/get-tag-counters-by-video-id/format/json?id=" + videoId;
    }
    else
        url = "checker/get-tag-counters-by-video-id/format/json?id=" + videoId;

    xmlhttp.open("POST", url, true);
    xmlhttp.send();
}

ValuesCloud.prototype.PutValuesIntoCloud = function(counters) {
    this.ResetValuesInCloud();    
    for(var x in counters)
    {        
        var sel = "#"+this.cloudId+">ul>li[title='"+counters[x].name+"']";        
        var li = $(sel);        
        $.each(li, function(i,o){
                  $(o).val(counters[x].counter);
                  $(o).children('button').html($(o).attr('title')+'('+counters[x].counter+')');
        });
    }
}

ValuesCloud.prototype.ResetValuesInCloud = function() {
    var li = $("#"+this.cloudId+">ul>li");
    $.each(li, function(i,o){
              $(o).val(0);
              $(o).children('button').html($(o).attr('title'));              
              // remove style for font-size, color.. eh
//              $(o).children('button').css('color',"");
              $(o).css('font-size',"");
    });    
}

ValuesCloud.prototype.HitTag = function(tagName) {
    var xmlhttp = createRequestObject();
    var url = document.domain;
    if (url.match(/^(.*)(localhost)(.*)/))
    {        
        url = "http://localhost/tuneTheMusic/public/index/increment-tag?videoId="+this.videoId+"&tagName="+tagName;
    }
    else
        url += "/index/increment-tag?videoId="+this.videoId+"&tagName="+tagName;

    xmlhttp.open("POST", url, true);
    xmlhttp.send();    
}