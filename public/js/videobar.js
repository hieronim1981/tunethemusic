function query(mood, handlerResponseFeed)
{
    var xmlhttp = createRequestObject();
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            var JSON_object = {};
            JSON_object = JSON.parse(xmlhttp.responseText);
            
            // fillVideoBar (after every selection of moods)
            // or takeNewFeed (auto feeding)
            handlerResponseFeed(JSON_object.feed);
        }
    }

    var url = document.domain;
    if (url.match(/^(.*)(localhost)(.*)/))
    {        
        url = "http://localhost/tuneTheMusic/public/index/music-by-tags?format=json&tags="+mood;
    }
    else
        url = "index/music-by-tags?format=json&tags="+mood;
    
    url+= "&feeded=" + feeded;
    ++feeded;
    
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

var feeded=0;
var feed_container;//{"feed":[{"id":"-0W-Gjx3TfA"},{"id":"-5A43pIuGKY"},{"id":"-R6DMruFqvE"},{"id":"03lItdEAhvo"},{"id":"0_7G-2Ea4GU"},{"id":"11WrSYuee84"},{"id":"15TSEKXXIvI"},{"id":"1cTcBzAjfsc"},{"id":"1RqT9rg1s-4"},{"id":"1Sc68Q3dlc8"},{"id":"21OEPqenwzc"},{"id":"29zQGhnKQck"},{"id":"2AzpHvLWFUM"},{"id":"2UIptI2rcjg"},{"id":"2WX_4FNoto4"},{"id":"3MoUgZ3XAPo"},{"id":"3Qb0KdwCvSs"},{"id":"3rMRGC_NEcg"},{"id":"3w2Oeww5WD0"},{"id":"3x_5eZjlrsE"}]};
var listSize = null;
var firstShown = null;
var lastShown = null;

function fillVideoBar(firstFeed)
{
    listSize = 8;
    feed_container = firstFeed;
    
    $(".onListButton").show();
    
    $("#videoBar ol>li").remove();
    
    for(var i=0; i<listSize; ++i)
    {
        $("#videoBar ol").append(
            $('<li>').attr('videoId', feed_container[i]['id'])
                .attr('trackNo', i)
                .click( function () {
                        videoChange(this);
                    }
                )
                .append($('<img>').attr('src', 'http://i4.ytimg.com/vi/'+feed_container[i]['id']+'/default.jpg').attr('alt', feed_container[i]['title']?feed_container[i]['title']:'unknown video')
            )
        );
    }            
    firstShown = 0;
    lastShown = listSize-1;
    
    $("videoBar ol>li").ready(function() {
            if(!playerLoaded)
                $("#ytFrame").show();
            else
                videoChange($("#videoBar ol>li:visible:first"));
        }    
    );
}

function takeNewFeed(newFeed)
{
    // DLACZEGO TUTAJ NIE DZIAŁA?
    //feed_cotainer.push.apply(feed_container, newFeed);
    
    for(var i in newFeed)
        feed_container.push(newFeed[i]);    
}

var currLiElem = null;

// user change or auto change (loading next video)
function videoChange(liElem)
{
    if(!liElem) // liElem is null when new feed was taken, because currLiElem.next() returned null
        liElem = $("#videobar >ol>li:visible:first");
    
    // set current Li to played
    if(currLiElem) // while loading first video, currLielem is null
    {
        $(currLiElem).children('img').removeClass('playing');
        $(currLiElem).children('img').addClass('played');
    }
    
    // set chosen Li to playing
    $(liElem).children('img').addClass('playing');

    // play video in frame !!!
    player.loadVideoById($(liElem).attr('videoId'), 0, 'large');
    
    // load values to cloud ..
    valuesCloudObject.GetTagCountersByVideoId($(liElem).attr('videoId'))
    
    // set chosen liElem to currLiElem   
    currLiElem = liElem;
}

//// POBRAĆ I WYŚWIETLAĆ TYTUŁ!
//$("#videoBar >li").ready( function(){
//    alert("videoBar >li ready");
//    $("#videoBar ol>li").click(
//                function() {
//                    alert($(this).attr('videoId'));
//                    
//                    $(this).toggleClass('playing');
//                }
//            );
//});


//przy przewijaniu listy do przodu chowamy elementy Li z początku video baru!

function nextOnList()
{
    // if needed get new feed in some advance...
    // if(lastShown == feed_container.length-2) {
    if(lastShown == feed_container.length-5) {
        query(moodsToString(), takeNewFeed);
    }
    
    // remove first item
    $("#videoBar ol>li:visible:first").hide();
    
    // add feed_container entry to list    
    $("#videoBar ol").append(
        $('<li>').attr('videoId', feed_container[lastShown+1]['id'])
                    .attr('trackNo', lastShown+1)
                    .click( function () {
                            videoChange(this);
                        }
                    )
                    .append($('<img>').attr('src', 'http://i4.ytimg.com/vi/'+feed_container[lastShown+1]['id']+'/default.jpg').attr('alt', feed_container[lastShown+1]['title']?feed_container[lastShown+1]['title']:'unknown video')
                )
    );
    
    ++firstShown;
    ++lastShown;
}

function prevOnList()
{
    if(firstShown>0)
    {
        // remove last item
        $("#videoBar ol>li:last").remove();

        // add prev entry to list
        $("#videoBar ol>li:hidden:last").show();
//        $("#videoBar ol").prepend(
//                        $('<li>').attr('videoId', feed_container[firstShown-1]['id']).append(
//                            $('<img>').attr('src', 'http://i4.ytimg.com/vi/'+feed_container[firstShown-1]['id']+'/default.jpg').attr('alt', feed_container[firstShown-1]['title']?feed_container[firstShown-1]['title']:'unknown video')
//                        )
//                    );

        --firstShown;
        --lastShown;
    }
}
